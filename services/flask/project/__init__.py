import os

from flask import Flask  # new
from flask_sqlalchemy import SQLAlchemy
from flask_admin import Admin
from flask_cors import CORS  # new
from flask_bcrypt import Bcrypt

# instantiate the db
db = SQLAlchemy()
admin = Admin(template_mode="bootstrap3")
cors = CORS()
bcrypt = Bcrypt()


def create_app(script_info=None):

    # instantiate the app
    app = Flask(__name__)

    # set config
    app_settings = os.getenv("APP_SETTINGS")
    app.config.from_object(app_settings)
    cors.init_app(app, resources={r"*": {"origins": "*"}})
    bcrypt.init_app(app)
    # set up extensions
    db.init_app(app)

    if os.getenv("FLASK_ENV") == "development":
        admin.init_app(app)

    # register blueprints
    from project.api import api

    api.init_app(app)

    # shell context for flask cli
    @app.shell_context_processor
    def ctx():
        return {"app": app, "db": db}

    return app
