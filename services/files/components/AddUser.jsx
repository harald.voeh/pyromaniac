import React from "react";
import PropTypes from "prop-types";

const AddUser = props => {
  return (
    <form onSubmit={props.handleSubmit}>
      <div className="field">
        <label htmlFor="input-username" className="label is-large">
          Username
        </label>
      </div>
      <input
        type="text"
        name="username"
        id="input-username"
        className="input is-large"
        placeholder="Enter a username"
        value={props.username}
        onChange={props.onChange}
        required
      />
      <div className="field">
        <label className="label is-large" htmlFor="input-email">
          Email
        </label>
        <input
          name="email"
          id="input-email"
          className="input is-large"
          type="email"
          placeholder="Enter an email address"
          value={props.email}
          onChange={props.onChange}
          required
        />
      </div>
      <input
        type="submit"
        className="button is-primary is-large is-fullwidth"
        value="Submit"
      />
    </form>
  );
};

AddUser.propTypes = {
  username: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired
};

export default AddUser;
